package com.mtag.showcases.springboot;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class EnvironmentVariableController {
    private final Counter demoCounter;
    private final AtomicInteger demoGauge;

    @Autowired
    private Environment env;
    private final AtomicLong counter = new AtomicLong();

    public EnvironmentVariableController(MeterRegistry registry) {
        this.demoCounter = registry.counter("demo_counter");
        this.demoGauge = registry.gauge("demo_gauge", new AtomicInteger(0));    }

    @GetMapping("/envvar")

    public String envvar() {
        return env.getProperty("envvar");
    }
}