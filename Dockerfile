FROM registry.access.redhat.com/ubi8/openjdk-11

LABEL org.label-schema.build-date=$BUILD_DATE \
    org.opencontainers.image.usage="Spring Boot App" \
    org.opencontainers.image.authors="konrad.heimel@mt-ag.com"

COPY ./target/*.jar /app/app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/app.jar"]

